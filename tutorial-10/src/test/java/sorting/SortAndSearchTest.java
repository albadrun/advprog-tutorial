package sorting;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;


public class SortAndSearchTest {
    int[] array;

    @Before
    public void setUp() {
        array = new int[] {2, 0, 1, 3, 4};
    }

    @Test
    public void testSlowSort() {
        int[] slowSorted = Sorter.slowSort(array);
        for (int i = 0; i < 5; i++) {
            assertEquals(i, slowSorted[i]);
        }
    }

    @Test
    public void testFastSort() {
        int[] fastSorted = Sorter.fastSort(array);
        for (int i = 0; i < 5; i++) {
            assertEquals(i, fastSorted[i]);
        }
    }

    @Test
    public void testSlowSearch() {
        int found = Finder.slowSearch(array, 4);
        int notFound = Finder.slowSearch(array, 5);
        assertEquals(4, found);
        assertEquals(-1, notFound);
    }

    @Test
    public void testFastSearch() {
        array = Sorter.fastSort(array);
        int found = Finder.fastSearch(array, 4);
        int notFound = Finder.fastSearch(array, 5);
        assertEquals(4, found);
        assertEquals(-1, notFound);
    }
}
