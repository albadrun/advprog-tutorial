package id.ac.ui.cs.advprog.tutorial4.exercise1;

import static org.junit.jupiter.api.Assertions.assertNotNull;

import id.ac.ui.cs.advprog.tutorial4.exercise1.pizza.Pizza;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class NewYorkPizzaStoreTest {

    PizzaStore nyStore;

    @BeforeEach
    void setUp() {
        nyStore = new NewYorkPizzaStore();
    }

    @Test
    void createPizza() {
        Pizza pizza = nyStore.orderPizza("cheese");
        assertNotNull(pizza);

        pizza = nyStore.orderPizza("clam");
        assertNotNull(pizza);

        pizza = nyStore.orderPizza("veggie");
        assertNotNull(pizza);
    }
}