package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies;

public class Nuts implements Veggies {

    public String toString() {
        return "Nuts";
    }
}
