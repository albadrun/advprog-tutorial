package tallycounter;

/*
import java.util.concurrent.atomic.AtomicInteger;

// Atomic Integer
public class TallyCounter {
    private AtomicInteger counter = new AtomicInteger();

    public void increment() {
        counter.incrementAndGet();
    }

    public void decrement() {
        counter.decrementAndGet();
    }

    public int value() {
        return counter.intValue();
    }
}
 */

// Using synchronized
public class TallyCounter {
    private int counter = 0;

    public void increment() {
        synchronized (this) {
            counter++;
        }
    }

    public void decrement() {
        synchronized (this) {
            counter--;
        }
    }

    public int value() {
        return counter;
    }
}


