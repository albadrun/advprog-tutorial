package tutorial.javari;

import com.opencsv.CSVWriter;
import com.opencsv.bean.CsvToBean;
import com.opencsv.bean.CsvToBeanBuilder;
import com.opencsv.bean.StatefulBeanToCsv;
import com.opencsv.bean.StatefulBeanToCsvBuilder;
import tutorial.javari.animal.Animal;
import tutorial.javari.animal.FlattenAnimal;

import java.io.Reader;
import java.io.Writer;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Collectors;

public class AnimalCSVParser {
    private static final String ANIMAL_LIST = "animal-list.csv";

    public synchronized static void write(ArrayList<Animal> animals) {

        try (
                Writer writer = Files.newBufferedWriter(Paths.get(ANIMAL_LIST));
        ) {
            StatefulBeanToCsv<FlattenAnimal> beanToCsv = new StatefulBeanToCsvBuilder(writer)
                    .withQuotechar(CSVWriter.NO_QUOTE_CHARACTER)
                    .build();

            List<FlattenAnimal> flattenAnimalsnimals = animals.stream()
                    .map(i -> new FlattenAnimal(i))
                    .collect(Collectors.toList());

            beanToCsv.write(flattenAnimalsnimals);
        } catch (Exception e) {
            System.out.println(e.toString());
        }
    }

    public synchronized static ArrayList<Animal> read() {
        ArrayList<Animal> animals = new ArrayList<>();
        try (
                Reader reader = Files.newBufferedReader(Paths.get(ANIMAL_LIST));
        ) {
            CsvToBean<FlattenAnimal> csvToBean = new CsvToBeanBuilder(reader)
                    .withType(FlattenAnimal.class)
                    .withIgnoreLeadingWhiteSpace(true)
                    .build();

            Iterator<FlattenAnimal> flattenAnimalIterator = csvToBean.iterator();
            flattenAnimalIterator.forEachRemaining(flattenAnimal -> animals.add(flattenAnimal.toAnimal()));

        } catch (Exception e) {
            System.out.println(e.toString());
        }
        return animals;
    }

}
