package tutorial.javari.animal;

import com.opencsv.bean.CsvBindByPosition;

public class FlattenAnimal {
    @CsvBindByPosition(position = 0)
    private int id;

    @CsvBindByPosition(position = 1)
    private String type;

    @CsvBindByPosition(position = 2)
    private String name;

    @CsvBindByPosition(position = 3)
    private String gender;

    @CsvBindByPosition(position = 4)
    private double length;

    @CsvBindByPosition(position = 5)
    private double weight;

    @CsvBindByPosition(position = 6)
    private String condition;

    public String getCondition() {
        return condition;
    }

    public void setCondition(String condition) {
        this.condition = condition;
    }

    public FlattenAnimal() {
    }

    public FlattenAnimal(int id, String type, String name, String gender, double length, double weight, String condition) {
        this.id = id;
        this.type = type;
        this.name = name;
        this.gender = gender;
        this.length = length;
        this.weight = weight;
        this.condition = condition;
    }

    public FlattenAnimal(Animal animal) {
        this.id = animal.getId();
        this.type = animal.getType();
        this.name = animal.getName();
        this.gender = animal.getGender().name();
        this.length = animal.getLength();
        this.weight = animal.getWeight();
        this.condition = animal.getCondition().name();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public double getLength() {
        return length;
    }

    public void setLength(double length) {
        this.length = length;
    }

    public double getWeight() {
        return weight;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }

    public Animal toAnimal() {
        return new Animal(id, type, name, Gender.parseGender(gender), length, weight, Condition.parseCondition(condition));
    }
}
